/****************************/
/** Import des modules  **/
const express = require("express")
const cors = require('cors')
/******************************************/
/** Import de la connexion à la database **/
let DB = require("./db.config")

/******************************************/
/** Import de la sécurité des routes **/
const checkTokenMiddleware = require("./jsonwebtoken/check")
const errorHandler = require('./error/errorHandler')
/****************************/
/** Initialisatioon de l'API  **/
const app = express()

app.use(cors({
    origin: "*",
    methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
    allowedHeaders: "Origin, X-Requested-With, x-access-token, role, Content, Accept, Content-Type, Authorization"
}))

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

/****************************/
/** Importation des routes **/
const user_router = require('./routes/users')
const billet_router = require('./routes/billets')
const auth_router = require('./routes/auth')

/****************************/
/** Mise en place du routage **/
app.get("/", (req, res) => res.send("j'im online , Welcome"))
app.use('/users', user_router)
app.use('/billets', billet_router)
app.use('/auth', auth_router)

app.get("*", (req, res) => res.status(501).send("What the hell are you doing ?"))

app.use(errorHandler)
/**********************************************************/
/** Lancement du serveur avec test de la base de donnée **/
DB.sequelize.authenticate()
    .then(() => console.log('database ok'))
    .then(() => {
        app.listen(process.env.SERVER_PORT, () => {
            console.log(`le serveur est lancé sur le port ${process.env.SERVER_PORT}`)
        })
    })
    .catch(err => console.log('database Error', err))

