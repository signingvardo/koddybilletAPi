/*************************/
/** Import des modules  **/
const express = require('express')
const billetCtrl = require('../controllers/billet')
/***************************************/
/** Récupération du router d'express  **/
let router = express.Router()

/******************************************/
/** Import de la sécurité des routes **/
const checkTokenMiddleware = require("../jsonwebtoken/check")

/***********************************/
/** Routage de la ressource User  **/

router.get('/', billetCtrl.getAllBillet)
//Recuperation d'un utilisateur en fonction de son id
router.get('/:id',billetCtrl.getBilletById)

//Creation d'un utilisateur 
router.put('',checkTokenMiddleware, billetCtrl.addBillet)

//Mise à jour des informations d'un utilisateur
router.patch('/:id',checkTokenMiddleware, billetCtrl.updateBillet)

//Mise d'un utilisateur dans la corbeille
router.delete('/trash/:id',checkTokenMiddleware, billetCtrl.trashBillet)

//Restauration d'un utilisateur depuis la corbeille
router.post('/untrash/:id',checkTokenMiddleware, billetCtrl.onTrashBillet)

//Suppression definitive d'un utilisateur
router.delete('/:id',checkTokenMiddleware, billetCtrl.deleteBillet)

module.exports = router