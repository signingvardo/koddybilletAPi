/*************************/
/** Import des modules  **/
const express = require('express')
const authCtrl = require('../controllers/auth')

/***************************************/
/** Récupération du router d'express  **/
let router = express.Router()

/***********************************/
/** Routage de la ressource Auth  **/

router.post('/login', authCtrl.login)

module.exports = router