/*************************/
/** Import des modules  **/
const express = require('express')
const userCtrl = require('../controllers/user')
/***************************************/
/** Récupération du router d'express  **/
let router = express.Router()

/***********************************/
/** Routage de la ressource User  **/

router.get('/', userCtrl.getAllUsers)
//Recuperation d'un utilisateur en fonction de son id
router.get('/:id',userCtrl.getUserById)

//Creation d'un utilisateur 
router.put('', userCtrl.addUsers)

//Mise à jour des informations d'un utilisateur
router.patch('/:id', userCtrl.updateUser)

//Mise d'un utilisateur dans la corbeille
router.delete('/trash/:id', userCtrl.trashUser)

//Restauration d'un utilisateur depuis la corbeille
router.post('/untrash/:id', userCtrl.untrashUser)

//Suppression definitive d'un utilisateur
router.delete('/:id', userCtrl.deleteUser)

module.exports = router