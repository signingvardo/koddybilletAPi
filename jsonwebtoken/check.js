/*************************/
/** Import des modules  **/
const jwt = require('jsonwebtoken')
/*************************/
/** Extration du token  **/
const extractBearer = authorization =>{
    if (typeof authorization !== 'string') {
        return false
    }
    // on isole le token
    const macthes = authorization.match(/(bearer)\s+(\S+)/i)
    return macthes && macthes[2]
}

/**********************************************/
/** Verification de la pr"sence d'un token  **/
const checkTokenMiddleware = (req,res,next)=>{
    
    const token = req.headers.authorization && extractBearer(req.headers.authorization)
    if (!token) {
        return res.status(401).json({message:"vous n'etez pas connectés"})
    }

    //Verifier la validiité du token
    jwt.verify(token, process.env.JWT_SECRET,(err,decodedToken)=>{
        if (err) {
            return res.status(401).json({message:'Bad token'})
        }

        next()
    })
    // next()
}
module.exports = checkTokenMiddleware