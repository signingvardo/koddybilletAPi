/*************************/
/** Import des modules  **/
const { DataTypes } = require("sequelize")
// const DB= require ('../db.config')
const bcrypt = require('bcrypt')
/*************************/
/** Definition du model user  **/
module.exports = (sequelize) => {
    const User = sequelize.define('User', {
        id: {
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            type: DataTypes.UUID,
        },
        sexe: {
            type: DataTypes.STRING(100),
            defaultValue: '',
            allowNull: false
        },
        nom: {
            type: DataTypes.STRING(100),
            defaultValue: '',
            allowNull: false
        },
        prenom: {
            type: DataTypes.STRING(100),
            defaultValue: '',
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            validate: {
                isEmail: true // validation d'une email
            }
        },
        password: {
            type: DataTypes.STRING(64),
            is: /^[0-9a-f]{64}$/i //contrainte d'un mot de passe de mot de passe
        },
        mobile: {
            type: DataTypes.INTEGER(9),
            allowNull: false,
            // unique: true
        },
        ville: {
            type: DataTypes.STRING(100),
            allowNull: false,
            defaultValue: ""
        },
        pays: {
            type: DataTypes.STRING(100),
            allowNull: false,
            defaultValue: ""
        },

    }, { paranoid: true }) // softDelete (mise a la corbeille ou alors suppression non definitive)
   
    User.beforeCreate(async (user, options) => {
        let hash = await bcrypt.hash(user.password, parseInt(process.env.BCRYPT_SALT_ROUND))
        user.password = hash
    })

    User.checkPassword = async (password, originel) => {
        return await bcrypt.compare(password, originel)
    }
    return User

}
/*******************************/
/** Synchronisation du model **/
// User.sync()
//User.sync({force:true})
//User.sync({alter:true})
// module.exports = User