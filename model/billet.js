/*************************/
/** Import des modules  **/
const { DataTypes } = require("sequelize")
// const DB= require ('../db.config')

/*************************/
/** Definition du model user  **/
module.exports = (sequelize) => {
    return Evenement = sequelize.define('Evenement', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
        },
        user_id: {
            type: DataTypes.UUID,
            allowNull: false
        },
        Photo: {
            type: DataTypes.TEXT("long"),
            defaultValue: '',
            allowNull: false
        },
        code: {
            type: DataTypes.TEXT("long"),
            defaultValue: '',
            allowNull: false
        },
        Titre: {
            type: DataTypes.STRING(100),
            defaultValue: '',
            allowNull: false
        },
        Description: {
            type: DataTypes.TEXT("long"),
            defaultValue: '',
            allowNull: false
        },
        Categorie: {
            type: DataTypes.STRING(100),
            defaultValue: '',
            allowNull: false
        },
        Remise: {
            type: DataTypes.STRING(100),
            allowNull: false,
            defaultValue: ""
        },
        Quantite: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
        },
        prix: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
        },
        date: {
            type: DataTypes.DATE,
            allowNull: false,
        }
    }, { paranoid: true }) // softDelete (mise a la corbeille ou alors suppression non definitive)
}
/*******************************/
/** Synchronisation du model **/
// Evenement.sync()
//User.sync({force:true})
//User.sync({alter:true})
// module.exports = Evenement