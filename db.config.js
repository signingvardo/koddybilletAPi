/**************************/
/** Import des modules  **/
const {Sequelize}= require('sequelize')

/*************************************/
/** Connexion à la base de donnée  **/
let sequelize = new Sequelize(
    process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
        host: process.env.HOST,
        port: process.env.DB_PORT,
        dialect:'mysql',
        logging:false
    }
)
/*** Mise en place des relations */
const db = {}

db.sequelize = sequelize
db.User = require('./model/user')(sequelize)
db.Evenement = require('./model/billet')(sequelize)

db.User.hasMany(db.Evenement, {foreignKey: 'user_id', onDelete: 'cascade'})
db.Evenement.belongsTo(db.User, {foreignKey: 'user_id'})

/**********************************/
/** Synchronisation des modules **/
db.sequelize.sync({alter: true})
module.exports = db