/*************************/
/** Import des modules  **/
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const DB = require('../db.config')
const User = DB.User
// const User = require("../model/user")
const { AuthenticationError } = require('../error/customError')
/***********************************/
/** Routage de la ressource Auth  **/

exports.login = async (req, res, next) => {
    const {email, password} = req.body
 
    try {
        //Vérification si le champ l'id est présent et cohérant
        if (!email || !password) {
            throw new AuthenticationError('Mauvais email ou mot de passe', 0)
        }
        //Récupération de l'utilisateur
        let user = await User.findOne({ where: { email: email }, raw: true })
       
        //Vérification si l'utilisateur exite déjà
        if (user === null) {
            throw new AuthenticationError("Cet utilisateur n'existe pas !", 1)
        }

        //Vérification du mot de passe
        // let test = bcrypt.compare(password, user.password)
        let test = await User.checkPassword(password, user.password)
        if (!test) {
            throw new AuthenticationError('Mauvais mot de passe', 2)
        }
        //Génération du token
        const token = jwt.sign({
            id:user.id,                       
            nom:user.nom,                       
            prenom:user.prenom,                       
            email:user.email,                       
        }, process.env.JWT_SECRET,{ expiresIn: process.env.JWT_DURING})

        return res.json({access_token:token})

    } catch (err) {
        next(err)
    }
}