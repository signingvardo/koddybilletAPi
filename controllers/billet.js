/*************************/
/** Import des modules  **/
const bcrypt = require('bcrypt')
// const Billet = require("../model/billet")
const DB = require('../db.config')
const Billet = DB.Evenement
const User = DB.User
const { RequestError, BilletError } = require('../error/customError')
/***********************************/
/** Routage de la ressource User  **/

//Recuperation de l'ensemble des utilisateurs
exports.getAllBillet = async (req, res) => {
    try {
        let billet = await Billet.findAll()
        return res.json({ data: billet })
    } catch (err) {
        return res.status(500).json({ message: 'Database Error', Error: err })
    }

}

//Recuperation d'un utilisateur en fonction de son id
exports.getBilletById = async (req, res, next) => {
    let billetId = req.params.id

    //Vérification si le champ l'id est présent et cohérant
    if (!billetId) {
        throw new RequestError('Absence de paramètre')
    }
    try {
        //Récupération de l'utilisateur
        let billet = await Billet.findOne({ where: { id: billetId }, include: {model: User } })

        //Vérification si l'utilisateur exite déjà
        if ((billet === null)) {
            throw new BilletError("Ce billet n'existe pas !", 0)
        }

        //Utilisateur trouvé
        return res.json({ data: billet })
    } catch (err) {
        next(err)
    }
}

//Creation d'un utilisateur 
exports.addBillet = async (req, res, next) => {
    const { Titre,Photo,Description,Remise, prix, Quantite, Categorie, user_id } = req.body

    // Validation des donnés recues
    if (!prix || !Quantite || !Categorie || !Remise || !Description || !user_id || !Photo || !Titre) {
        return res.json(400).json({ message: "Absence de paramètre" })
    }

    try {
        let billet = await Billet.findOne({ where: { Titre: Titre }, raw: true })

        //Vérification si l'utilisateur exite déjà
        if ((billet !== null)) {
            throw new BilletError(`Le billet ${Titre} existe déjà !`, 1)
        }

        let billetC = await Billet.create(req.body)
        res.json({ message: "Billet crée", data: billetC })

    } catch (err) {
        next(err)
    }

}

//Mise à jour des informations d'un utilisateur
exports.updateBillet = async (req, res, next) => {
    let biletId = req.params.id

    //Vérification si le champ l'id est présent et cohérant
    if (!biletId) {
        throw new RequestError('Absence de paramètre')
    }

    //Récupération de l'utilisateur
    try {
        let bilet = await Billet.findOne({ where: { id: biletId }, raw: true })

        //Vérification si l'utilisateur exite déjà
        if ((bilet === null)) {
            throw new BilletError("Ce billet n'existe pas !", 0)
        }

        await Billet.update(req.body, { where: { id: biletId } })
        res.json({ message: "Billet Modifié" })

    } catch (err) {
        next(err)
    }

}

//Mise d'un utilisateur dans la corbeille
exports.trashBillet = async (req, res, next) => {
    try {
        let biletId =req.params.id

        //Vérification si le champ l'id est présent et cohérant
        if (!biletId) {
            throw new RequestError('Absence de paramètre')
        }
        //Suppression de l'utilisateur
        await Billet.destroy({ where: { id: biletId } })
        return res.status(204).json({})
    } catch (err) {
        next(err)
    }
}

//Restauration d'un utilisateur depuis la corbeille
exports.onTrashBillet = async (req, res, next) => {
    try {
        let biletId = req.params.id

        //Vérification si le champ l'id est présent et cohérant
        if (!biletId) {
            throw new RequestError('Absence de paramètre')
        }
        //Suppression de l'utilisateur
        await Billet.restore({ where: { id: biletId } })
        return res.status(204).json({})
    } catch (err) {
        next(err)
    }
}

//Suppression definitive d'un utilisateur
exports.deleteBillet = async (req, res, next) => {
    try {
        let biletId = req.params.id

        //Vérification si le champ l'id est présent et cohérant
        if (!biletId) {
            throw new RequestError('Absence de paramètre')
        }
        //Suppression de l'utilisateur
        await Billet.destroy({ where: { id: biletId } })
        return res.status(204).json({})
    } catch (err) {
        next(err)
    }
}