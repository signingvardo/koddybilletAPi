/*************************/
/** Import des modules  **/
const bcrypt = require('bcrypt')
// const User = require("../model/user")
const DB = require('../db.config')
const User = DB.User
const { RequestError, UserError } = require('../error/customError')

/***********************************/
/** Routage de la ressource User  **/

//Recuperation de l'ensemble des utilisateurs
exports.getAllUsers = async (req, res) => {
    try {
        let users = await User.findAll()
        return res.json({ data: users })
    } catch (err) {
        return res.status(500).json({ message: 'Database Error', Error: err })
    }
    
}

//Recuperation d'un utilisateur en fonction de son id
exports.getUserById = async (req, res,next) => {
    let userId = req.params.id

    //Vérification si le champ l'id est présent et cohérant
    if (!userId) {
        throw new RequestError('Absence de paramètre')
    }
    try {
        //Récupération de l'utilisateur
        let user = await User.findOne({ where: { id: userId }, raw: true })

         //Vérification si l'utilisateur exite déjà
         if ((user === null)) {
            throw new UserError("Cet utilisateur n'existe pas !", 0)
        }

        //Utilisateur trouvé
        return res.json({ data: user })
    } catch (err) {
        next(err)
    }
}

//Creation d'un utilisateur 
exports.addUsers = async (req, res,next) => {
    const { nom, prenom, email, password,mobile,ville,sexe } = req.body

    // Validation des donnés recues
    if (!nom||!prenom||!email||!password||!ville||!sexe||!mobile) {
        throw new RequestError('Absence de paramètre')
    }

    try {
        let user = await User.findOne({ where: { email: email }, raw: true })
       
        //Vérification si l'utilisateur exite déjà
        if ((user !== null)) {
            throw new UserError(`L'utilisateur ${nom} existe déjà !`, 1)
        }

        // Hashage du mot de passe utilisateur
        // let hash = await bcrypt.hash(password, parseInt(process.env.BCRYPT_SALT_ROUND))
        // req.body.password = hash

        let userC = await User.create(req.body)
        res.json({ message: "Utilisateur creé", data: userC })

    } catch (err) {
        next(err)   
    }

}

//Mise à jour des informations d'un utilisateur
exports.updateUser = async (req, res, next) => {
    let userId = req.params.id

    //Vérification si le champ l'id est présent et cohérant
    if (!userId) {
        throw new RequestError('Absence de paramètre')
    }

    //Récupération de l'utilisateur
    try {
        let user = await User.findOne({ where: { id: userId }, raw: true })
       
        //Vérification si l'utilisateur exite déjà
        if ((user === null)) {
            throw new UserError("Cet utilisateur n'existe pas !", 0)
        }

        await User.update(req.body,{ where: { id: userId }})
        res.json({ message: "Utilisateur modifié avec sucèss"})

    } catch (err) {
        next(err)   
    }

}

//Mise d'un utilisateur dans la corbeille
exports.trashUser = async (req, res, next) => {
    try {
        let userId = req.params.id

        // Vérification si le champ id est présent et cohérent
        if (!userId) {
            throw new RequestError('Absence de paramètre')
        }

        // Suppression de l'utilisateur
        await User.destroy({ where: { id: userId } })

        // Réponse de la mise en poubelle
        return res.status(204).json({})
    } catch (err) {
        next(err)
    }
}

//Restauration d'un utilisateur depuis la corbeille
exports.untrashUser = async (req, res, next) => {
    try {
        let userId = req.params.id

        // Vérification si le champ id est présent et cohérent
        if (!userId) {
            throw new RequestError('Absence de paramètre')
        }

        await User.restore({ where: { id: userId } })

        // Réponse de la sortie de poubelle
        return res.status(204).json({})
    } catch (err) {
        next(err)
    }
}

//Suppression definitive d'un utilisateur
exports.deleteUser = async (req, res, next) => {
    try {
        let userId = req.params.id

        // Vérification si le champ id est présent et cohérent
        if (!userId) {
            throw new RequestError('Absence de paramètre')
        }

        // Suppression de l'utilisateur
        await User.destroy({ where: { id: userId }, force: true })
        
        // Réponse de la suppression
        return res.status(204).json({})            
    } catch (err) {
        next(err)
    }
}